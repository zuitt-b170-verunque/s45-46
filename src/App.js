// base imports
import React, { useState } from 'react';
import {BrowserRouter as Router, Route, Routes  } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/index.css';


// App Components
import AppNavbar from './components/AppNavbar';

// App Imports
import userContext from './pages/userContext';

// Page Components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import PageNotFound from './components/PageNotFound';


export default function App() {

    const [user, setUser] = useState({email: localStorage.getItem('access')});

    // clears the local sotrage and clears the item
    const unsetUser = () => {
        localStorage.clear();
        setUser({access:null})
    }

    return (
        <userContext.Provider value={{user, setUser, unsetUser}}>
            <Router>
                    <AppNavbar user={user} />    
                    <Routes>
                        <Route path ="/" element={<Home/>}/>
                        <Route path ="/courses" element={<Courses/>}/>
                        <Route path ="/register" element={<Register/>}/>
                        <Route path ="/login" element={<Login/>}/>
                        <Route path = "*" element={<PageNotFound/>}/>
                    </Routes>
            </Router>
        </userContext.Provider>
    )
}





