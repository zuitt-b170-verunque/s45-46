// set up import dependencies
import ReactDOM from 'react-dom';

import App from './App.js';
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// removed everything except these two above
// changed 'react-dom/client'; >>> 'react-dom'; 


/* 
ReactDOM.render()
  responsible for injecting/inserting the whole React.js Project inside the webpage
*/
// deprecated version of the render() in React
// React.createElement('h1', null, "Hello World");

ReactDOM.render(
 <App/>

,document.getElementById('root')
);

