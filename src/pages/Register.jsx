// base imports
import React, {useState, useEffect, useContext} from 'react'
import {Navigate} from 'react-router-dom'

// App Import
import userContext from './userContext'

// bootstrap
import {Form, Container, Button}  from 'react-bootstrap'

import Swal from 'sweetalert2'

export default function Register() {
    // useState can be used to hold values much like a container
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [ passwordConfirm, setPasswordConfirm] = useState('')
    const [ firstName, setFirstName] = useState('')
    const [ lastName, setLastName] = useState('')
    const [ age, setAge] = useState('')
    const [ gender, setGender] = useState('')
    const [ mobileNo, setMobileNo] = useState('')
    const [ isDisabled, setIsDisabled] = useState(true)

    useEffect(() => {
        let isfirstNameNotEmpty = firstName !== ''
        let islastNameNotEmpty = lastName !== ''
        let mobileNoEnough = mobileNo.length >= `11`
        let isEmailNotEmpty = email !== ''
        let isPasswordNotEmpty = password !== ''
        let isPasswordConfirmNotEmpty = passwordConfirm !== ''
        let isPasswordMatch = password === passwordConfirm

        if (isfirstNameNotEmpty && islastNameNotEmpty && islastNameNotEmpty && mobileNoEnough && isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch) {
                setIsDisabled(false)
        } else {
                setIsDisabled(true)
        }
        
    },[firstName, lastName, mobileNo, email, password, passwordConfirm])


    function registered(e){
        e.preventDefault();
        fetch('http://localhost:4000/api/users/checkEmail', {
            method:'POST',
            headers:{"Content-Type" : "application/json"},
            body: JSON.stringify ({
                email:email
            })
        })
        .then(response => response.json())
        .then(response => {

            if(response ===true) {
                Swal.fire('Email already exists')
            }
            else {
                 // fetch from the server
        fetch('http://localhost:4000/api/users/register', {
            method: 'POST',
            headers:{"Content-Type" : "application/json"},
            body: JSON.stringify({ 
                firstName: firstName, 
                lastName: lastName, 
                age: age, 
                gender: gender, 
                mobileNo: mobileNo, 
                email: email, 
                password:password
            })
        })
        .then((response) => response.json())
        .then((response) => {

            if (response===true){
                Swal.fire(`Registered successfully, you may now log in`)
                // save the token inside localstorage
                localStorage.setItem('access', response.access);
                // data inside token is now stored inside setUser
                // access is the name of key - it is a token created in the back-end (from session 32-36, userController)
                setUser({access: response.access})
            } else {
                Swal.fire(`Registration failed`)
                setFirstName('');
                setLastName('');
                setAge('');
                setGender('');
                setMobileNo('');
                setEmail('');
                setPassword('');
                setPasswordConfirm('');
            }
        }) 
            }
        })


        
    }


    // redirect to courses
    const {user, setUser} = useContext(userContext);

    if (user.access !== null) {
        return <Navigate replace to="/login" />
    }


    return (
        <Container>
            <Form onSubmit={registered}>

                <Form.Group>
                    <Form.Label>First Name</Form.Label>
                    {/* value works like a variable */}
                    <Form.Control type = "text" placeholder='Enter First Name' value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Last Name</Form.Label>
                    {/* value works like a variable */}
                    <Form.Control type = "text" placeholder='Enter Last Name' value={lastName} onChange={(e) => setLastName(e.target.value)} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Age</Form.Label>
                    {/* value works like a variable */}
                    <Form.Control type = "text" placeholder='Enter Age' value={age} onChange={(e) => setAge(e.target.value)} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Gender</Form.Label>
                    {/* value works like a variable */}
                    <Form.Control type = "text" placeholder='Enter Gender' value={gender} onChange={(e) => setGender(e.target.value)} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Mobile No.</Form.Label>
                    {/* value works like a variable */}
                    <Form.Control type = "text" placeholder='Enter Mobile Number' value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    {/* value works like a variable */}
                    <Form.Control type = "email" placeholder='Enter Email' value={email} onChange={(e) => setEmail(e.target.value)} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type = "password" placeholder='password' value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>

                <Form.Group>
                    <Form.Label> Confirm Password</Form.Label>
                    <Form.Control type = "password" placeholder='password' value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                </Form.Group>

                <Button variant="primary" type="submit" disabled={isDisabled}> Submit </Button>
                
            </Form>
        </Container>
    )
}

