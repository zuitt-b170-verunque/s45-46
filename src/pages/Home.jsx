// dependencies
import React from 'react'


// BOOTSTRAP dependency
import Container from 'react-bootstrap/esm/Container'

// App Components
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
// import CourseCard from '../components/CourseCard'


export default function Home(){
    return(
        <Container fluid>
            <Banner />
            <Highlights />
        </Container>
    )
}