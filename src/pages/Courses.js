// Base imports
import React from 'react'
import Course from '../components/CourseCard'

// Bootstrap Dependency
import Container from 'react-bootstrap/Container'

// Data Import
import courses from '../mock-data/courses.js'


export default function Courses(){
    const CourseCards = courses.map((course) => {
        return (
           <Course course={course} /> 
        )
    })
    return (
        <Container fluid>
            {CourseCards}
        </Container>
    )
}
