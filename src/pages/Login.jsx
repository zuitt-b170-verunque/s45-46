import React, {useState, useEffect, useContext} from 'react'
import {Navigate} from 'react-router-dom'


// App Imports
import userContext from './userContext'

// bootstrap
import {Form, Container, Button}  from 'react-bootstrap'
import Swal from 'sweetalert2'



export default function Login() {

    const {user, setUser} = useContext(userContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [ isDisabled, setIsDisabled] = useState(true);


    useEffect(()=>{
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';

        if ( isEmailNotEmpty && isPasswordNotEmpty ) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
        },[ email, password ]);

    // log in function
    function login(e){
        e.preventDefault();

        // fetch from the server
        fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers:{"Content-Type" : "application/json"},
            body: JSON.stringify({ 
                email: email, 
                password:password
            })
        })
        .then((response) => response.json())
        .then((response) => {
            console.log(response)
            if (response.access !== undefined){
                Swal.fire(`You are logged in`)
                // save the token inside localstorage
                localStorage.setItem('access', response.access);
                // data inside token is now stored inside setUser
                // access is the name of key - it is a token created in the back-end (from session 32-36, userController)
                setUser({access: response.access})
            }
            else {
                Swal.fire(response.access)
                setEmail('')
                setPassword('')
            }
        })
    } 
    
    // redirect after logging in
    if (user.access !== null) {
        return <Navigate replace to="/" />
    }

    // return the HTML area
    return (

        <Container >
            <Form onSubmit={login}>

                <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control type = "email" placeholder='email' value={email} onChange={(e) => setEmail(e.target.value)}/>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control type = "password" placeholder='password' value={password} onChange={(e) => setPassword(e.target.value)}/>
                </Form.Group>

                <Button variant="success" type="submit" disabled={isDisabled}>Login</Button>

            </Form>
        </Container>
        
    )
}