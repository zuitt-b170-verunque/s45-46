export default [
	{
		id: "wdc001",
		courseName: "PHP - Laravel",
		description: "PHP ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut dui erat. Aenean a imperdiet mi. Curabitur eleifend ipsum sit amet fringilla hendrerit. Aliquam porttitor, magna quis feugiat convallis, mi nisl euismod nulla, id posuere ex mi nec felis.",
		price: 45000,
		onOffer: true
	},
{
		id: "wdc002",
		courseName: "Python - Django",
		description: "Python ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut dui erat. Aenean a imperdiet mi. Curabitur eleifend ipsum sit amet fringilla hendrerit. Aliquam porttitor, magna quis feugiat convallis, mi nisl euismod nulla, id posuere ex mi nec felis.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		courseName: "Java - Springboot",
		description: "Java ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut dui erat. Aenean a imperdiet mi. Curabitur eleifend ipsum sit amet fringilla hendrerit. Aliquam porttitor, magna quis feugiat convallis, mi nisl euismod nulla, id posuere ex mi nec felis.",
		price: 55000,
		onOffer: true
	}
]