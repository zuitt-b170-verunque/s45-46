import React, {useState, useEffect} from 'react'

import Container from 'react-bootstrap/esm/Container'


export default function Counter() {
    const [ count, setCount] = useState(0);

    /* 
        Effect hook A.K.A. useEffect() - allows us to execute a piece of code whenever a component gets rendered to the page or if the value of a state changes
                                     - it is used to make the page or at least part of the page reactive
        useEffect() - requires two arguments: function and the array
            function - to specify the codes to be executed
            array    - to set which variable is to be listened to, in terms of changing the state, so the function will be executed
    */
    useEffect(() =>{
        document.title = `${count} times`
    }, [count])

    return(
        <Container fluid>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count+1)}>Click Me!</button>
        </Container>
    )
}