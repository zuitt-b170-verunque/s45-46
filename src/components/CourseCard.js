// dependencies
import React, {useEffect, useState} from 'react'

// bootstrap components
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'



export default function CourseCard(props){

    let course = props.course; 

    // useState() allows components to create/manage its own data and is meant to be used internally
    const [isDisabled, setIsDisabled] = useState(0);
    const [seats, setSeats] = useState(10);

    useEffect(() => {
        if(seats === 0){
            setIsDisabled(true)
        }
    },[seats])


        return (
            <Card style={{ width: '100%' }} className="mt-3">
                <Card.Body>
    
                    {/* STATIC */}
                    
                    {/* <Card.Title>
                        Sample Course
                    </Card.Title>
                     <h6>Description:</h6>
                    <p>This is a sample course offering.</p>
    
                    <h6>Price</h6>
                    <p>Php 40,000.00</p>

                    <h6>Enrollees</h6>
                    <p>{count} Enrollees</p> 

                    <Button variant="primary" onClick={enroll} > Enroll! </Button> */}
    
                    {/* DYNAMIC */}
                    
                    <Card.Title>
                        {course.courseName}
                    </Card.Title>
                    <h6>Description:</h6>
                    <p>{course.description}.</p>
    
                    <h6>Price</h6>
                    <p>{course.price}</p> 

                    <h6>Seats</h6>
                    <p>{seats} remaining</p> 
                    <Button variant="primary" onClick={() => setSeats(seats -1)} disabled={isDisabled}> Enroll! </Button>
                   
    
                </Card.Body>
            </Card>
        )
    
}