import React from 'react';
import Container from 'react-bootstrap/esm/Container'

export default function PageNotFound(){
    

    return (
        <Container fluid>
            <h1>404 Error</h1>
            <h1>Page Not Found</h1>
            <p>Go back to <a href='/'>homepage</a></p>
        </Container>
    )
}
